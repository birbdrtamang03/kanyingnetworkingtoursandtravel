<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Ka-Nying</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <!-- <link href="{{asset('img/favicon.ico')}}" rel="icon"> -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<!--Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
  <!-- Propeller CSS -->  
  <link rel="stylesheet" href="css/propeller.min.css">
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet"> 

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css')}}" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="floating-wpp.min.js"></script>
    <link rel="stylesheet" href="floating-wpp.min.css">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" /> -->

</head>

<body>
    <!-- Topbar Start -->
    <div class="container-fluid bg-light pt-3 d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left mb-2 mb-lg-0">
                    <div class="d-inline-flex align-items-center">
                        <p><i class="fa fa-envelope mr-2"></i>dorjitshewang49@gmail.com</p>
                        <p class="text-body px-3">|</p>
                        <p><i class="fa fa-phone-alt mr-2"></i>+975 17407293</p>
                    </div>
                </div>
                <div class="col-lg-6 text-center text-lg-right">
                    <div class="d-inline-flex align-items-center">
                        <a class="text-primary px-3" href="https://www.facebook.com/kanyingnetworkingtours">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-primary px-3" href="https://twitter.com/kanying17407293">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 14 14"><g fill="none"><g clip-path="url(#primeTwitter0)"><path fill="currentColor" d="M11.025.656h2.147L8.482 6.03L14 13.344H9.68L6.294 8.909l-3.87 4.435H.275l5.016-5.75L0 .657h4.43L7.486 4.71zm-.755 11.4h1.19L3.78 1.877H2.504z"/></g><defs><clipPath id="primeTwitter0"><path fill="#fff" d="M0 0h14v14H0z"/></clipPath></defs></g></svg>
                        </a>
                        <a class="text-primary px-3" href="https://www.linkedin.com/in/ka-nying-networking-tours-and-travels-7b3285231/">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-primary px-3" href="https://www.instagram.com/kanyingnetworkingtour1992/">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a class="text-primary px-3" href="https://wa.link/hslp9n">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid position-relative nav-bar p-0">
        <div class="container-lg position-relative p-0 px-lg-3" style="z-index: 9;">
            <nav class="navbar navbar-expand-lg bg-light navbar-light shadow-lg py-3 py-lg-0 pl-3 pl-lg-5">
                <a href="" class="navbar-brand">
                    <h1 class="m-0 text-primary" style="font-size:larger"><span class="text-dark">Ka-</span>Nying</h1>
                    <span style="font-size:small">Networking Tours and Travel</span>
                </a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between px-3" id="navbarCollapse">
                    <div class="navbar-nav ml-auto py-0">
                        <a href="#about" class="nav-item nav-link" style="color:#333">About</a>
                        <a href="#destinations" class="nav-item nav-link" style="color:#333">Destinations</a>
                        <a href="#services" class="nav-item nav-link" style="color:#333">Services</a>
                        <a href="#gallery" class="nav-item nav-link" style="color:#333">Gallery</a>
                        <a href="#feedback" class="nav-item nav-link" style="color:#333">Feedback</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Navbar End -->


    <!-- Carousel Start -->
    <div class="container-fluid p-0">
        <div id="header-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="w-100" src="img/herobanner.jpg" alt="Image">
                    <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h4 class="text-white text-uppercase mb-md-3">Tours & Travel</h4>
                            <h1 class="display-3 text-white mb-md-4">Let's Discover Bhutan's Wonderful Places</h1>
                            <a href="" class="btn btn-primary py-md-3 px-md-5 mt-2">Explore</a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="w-100" src="img/banner.jpg" alt="Image">
                    <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h4 class="text-white text-uppercase mb-md-3">Tours & Travel</h4>
                            <h1 class="display-3 text-white mb-md-4">Discover Amazing Places With Us</h1>
                            <a href="" class="btn btn-primary py-md-3 px-md-5 mt-2">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#header-carousel" data-slide="prev">
                <div class="btn btn-dark" style="width: 45px; height: 45px;">
                    <span class="carousel-control-prev-icon mb-n2"></span>
                </div>
            </a>
            <a class="carousel-control-next" href="#header-carousel" data-slide="next">
                <div class="btn btn-dark" style="width: 45px; height: 45px;">
                    <span class="carousel-control-next-icon mb-n2"></span>
                </div>
            </a>
        </div>
    </div>
    <!-- Carousel End -->


    <!-- Booking Start -->
    <div class="container-fluid booking mt-5 pb-5">
        <div class="container pb-5" >
            <div class="bg-light shadow" style="padding: 30px;">
                <div class="row" style="min-height: 60px;">
                    <form style="width:50%" class="max-w-sm mx-auto" style="width:100%;" action="{{route('bookTravel')}}" method="POST">
                    <h6 class="text-primary text-uppercase" style="letter-spacing: 5px; text-align:center;margin-bottom:20px">Book Travel Now</h6>
                        @csrf
                        <div class="form-group">
                            <input name="name" type="text" id="name" class="form-control" placeholder="Fullname" required />
                        </div>
                        <div class="form-group">
                            <input name="email" type="email" id="email"  class="form-control" placeholder="Email" required />
                        </div>
                        <div class="form-group">
                            <input name="address" type="text" id="address"  class="form-control" placeholder="Address" required />
                        </div>
                        <div class="form-group">
                            <input name="contact" type="number" id="contact"  class="form-control" placeholder="Contact No." required />
                        </div>
                        <div class="form-group">
                            <select name="tour_type" id="tour_type"  class="form-control">
                                <option value="Cultural Tour">Cultural Tour</option>
                                <option value="Trakking">Trakking</option>
                                <option value="Camping">Camping</option>
                                <option value="Bird Watching">Bird Watching</option>
                                <option value="Honeymoon">Honeymoon</option>
                            </select>
                        </div>
                        <textarea style="width:100%;margin-bottom:10px;padding-inline:10px" name="message" id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Leave a note..."></textarea>
                        <button type="submit" style="background-color:#7ab730" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- Booking End -->


    <!-- About Start -->
    <div id="about" class="container-fluid py-5">
        <h6 class="text-primary text-uppercase" style="letter-spacing: 5px; text-align:center;">Explore</h6>
        <h1 style="text-align:center;font-size:xx-large">About Us</h1>
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-6" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100" src="img/about.jpg" style="object-fit: cover;">
                    </div>
                </div>
                <div class="col-lg-6 pt-5 pb-lg-5">
                    <div class="about-text bg-white p-4 p-lg-5 my-lg-5">
                        <h1 class="mb-3">Learn More About Kanying</h1>
                        <p style="font-size:small">Ka-nying Agency is your premier destination for unparalleled service, competitive pricing, and comprehensive coverage across Bhutan. With a steadfast commitment to excellence, we pride ourselves on delivering top-tier solutions tailored to meet your every need. Our team is dedicated to providing the best services available in the market, ensuring that our clients receive nothing short of exceptional value. Whether you're seeking insurance, logistics, or specialized assistance, trust Ka-nying to deliver excellence with every interaction. Welcome to a world of reliability, where your satisfaction is our priority.</p>
                        <div class="row mb-4">
                            <div class="col-6">
                                <img class="img-fluid" src="img/about-1.jpg" alt="">
                            </div>
                            <div class="col-6">
                                <img class="img-fluid" src="img/about-2.jpg" alt="">
                            </div>
                        </div>
                        <!-- <a href="" class="btn btn-primary mt-1">Explore</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->


    <!-- Feature Start -->
    <div class="container-fluid pb-5">
        <div class="container pb-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="d-flex mb-4 mb-lg-0">
                        <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-primary mr-3" style="height: 100px; width: 100px;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.3em" height="2.3em" viewBox="0 0 128 128"><path fill="#ffca28" d="M93.46 39.45c6.71-1.49 15.45-8.15 16.78-11.43c.78-1.92-3.11-4.92-4.15-6.13c-2.38-2.76-1.42-4.12-.5-7.41c1.05-3.74-1.44-7.87-4.97-9.49s-7.75-1.11-11.3.47s-6.58 4.12-9.55 6.62c-2.17-1.37-5.63-7.42-11.23-3.49c-3.87 2.71-4.22 8.61-3.72 13.32c1.17 10.87 3.85 16.51 8.9 18.03c6.38 1.92 13.44.91 19.74-.49"/><path fill="#e2a610" d="M104.36 8.18c-.85 14.65-15.14 24.37-21.92 28.65l4.4 3.78s2.79.06 6.61-1.16c6.55-2.08 16.12-7.96 16.78-11.43c.97-5.05-4.21-3.95-5.38-7.94c-.61-2.11 2.97-6.1-.49-11.9m-24.58 3.91s-2.55-2.61-4.44-3.8c-.94 1.77-1.61 3.69-1.94 5.67c-.59 3.48 0 8.42 1.39 12.1c.22.57 1.04.48 1.13-.12c1.2-7.91 3.86-13.85 3.86-13.85"/><path fill="#ffca28" d="M61.96 38.16S30.77 41.53 16.7 68.61s-2.11 43.5 10.55 49.48c12.66 5.98 44.56 8.09 65.31 3.17s25.94-15.12 24.97-24.97c-1.41-14.38-14.77-23.22-14.77-23.22s.53-17.76-13.25-29.29c-12.23-10.24-27.55-5.62-27.55-5.62"/><path fill="#6b4b46" d="M74.76 83.73c-6.69-8.44-14.59-9.57-17.12-12.6c-1.38-1.65-2.19-3.32-1.88-5.39c.33-2.2 2.88-3.72 4.86-4.09c2.31-.44 7.82-.21 12.45 4.2c1.1 1.04.7 2.66.67 4.11c-.08 3.11 4.37 6.13 7.97 3.53c3.61-2.61.84-8.42-1.49-11.24c-1.76-2.13-8.14-6.82-16.07-7.56c-2.23-.21-11.2-1.54-16.38 8.31c-1.49 2.83-2.04 9.67 5.76 15.45c1.63 1.21 10.09 5.51 12.44 8.3c4.07 4.83 1.28 9.08-1.9 9.64c-8.67 1.52-13.58-3.17-14.49-5.74c-.65-1.83.03-3.81-.81-5.53c-.86-1.77-2.62-2.47-4.48-1.88c-6.1 1.94-4.16 8.61-1.46 12.28c2.89 3.93 6.44 6.3 10.43 7.6c14.89 4.85 22.05-2.81 23.3-8.42c.92-4.11.82-7.67-1.8-10.97"/><path fill="none" stroke="#6b4b46" stroke-miterlimit="10" stroke-width="5" d="M71.16 48.99c-12.67 27.06-14.85 61.23-14.85 61.23"/><path fill="#6d4c41" d="M81.67 31.96c8.44 2.75 10.31 10.38 9.7 12.46c-.73 2.44-10.08-7.06-23.98-6.49c-4.86.2-3.45-2.78-1.2-4.5c2.97-2.27 7.96-3.91 15.48-1.47"/><path fill="#6b4b46" d="M81.67 31.96c8.44 2.75 10.31 10.38 9.7 12.46c-.73 2.44-10.08-7.06-23.98-6.49c-4.86.2-3.45-2.78-1.2-4.5c2.97-2.27 7.96-3.91 15.48-1.47"/><path fill="#e2a610" d="M96.49 58.86c1.06-.73 4.62.53 5.62 7.5c.49 3.41.64 6.71.64 6.71s-4.2-3.77-5.59-6.42c-1.75-3.35-2.43-6.59-.67-7.79"/></svg>                        
                        </div>
                        <div class="d-flex flex-column">
                            <h5 class="">Competitive Pricing</h5>
                            <p class="m-0" style="font-size:small">Affordable Bhutan adventures without compromise, ensuring you get the most out of your budget.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex mb-4 mb-lg-0">
                        <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-primary mr-3" style="height: 100px; width: 100px;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.3em" height="2.3em" viewBox="0 0 48 48"><path fill="#e65100" d="M25.6 34.4c.1-.4.1-.9.1-1.4s0-.9-.1-1.4l2.8-2c.3-.2.4-.6.2-.9l-2.7-4.6c-.2-.3-.5-.4-.8-.3L22 25.3c-.7-.6-1.5-1-2.4-1.4l-.3-3.4c0-.3-.3-.6-.6-.6h-5.3c-.3 0-.6.3-.6.6l-.4 3.5c-.9.3-1.6.8-2.4 1.4L6.9 24c-.3-.1-.7 0-.8.3l-2.7 4.6c-.2.3-.1.7.2.9l2.8 2c-.1.4-.1.9-.1 1.4s0 .9.1 1.4l-2.8 2c-.3.2-.4.6-.2.9l2.7 4.6c.2.3.5.4.8.3L10 41c.7.6 1.5 1 2.4 1.4l.3 3.4c0 .3.3.6.6.6h5.3c.3 0 .6-.3.6-.6l.3-3.4c.9-.3 1.6-.8 2.4-1.4l3.1 1.4c.3.1.7 0 .8-.3l2.7-4.6c.2-.3.1-.7-.2-.9zM16 38c-2.8 0-5-2.2-5-5s2.2-5 5-5s5 2.2 5 5s-2.2 5-5 5"/><path fill="#ffa000" d="M41.9 15.3c.1-.5.1-.9.1-1.3s0-.8-.1-1.3l2.5-1.8c.3-.2.3-.5.2-.8l-2.5-4.3c-.2-.3-.5-.4-.8-.2l-2.9 1.3c-.7-.5-1.4-.9-2.2-1.3l-.3-3.1c.1-.3-.1-.5-.4-.5h-4.9c-.3 0-.6.2-.6.5l-.3 3.1c-.8.3-1.5.7-2.2 1.3l-2.9-1.3c-.3-.1-.6 0-.8.2l-2.5 4.3c-.2.3-.1.6.2.8l2.5 1.8V14c0 .4 0 .8.1 1.3l-2.5 1.8c-.3.2-.3.5-.2.8l2.5 4.3c.2.3.5.4.8.2l2.9-1.3c.7.5 1.4.9 2.2 1.3l.3 3.1c0 .3.3.5.6.5h4.9c.3 0 .6-.2.6-.5l.3-3.1c.8-.3 1.5-.7 2.2-1.3l2.9 1.3c.3.1.6 0 .8-.2l2.5-4.3c.2-.3.1-.6-.2-.8zM33 19c-2.8 0-5-2.2-5-5s2.2-5 5-5s5 2.2 5 5s-2.2 5-5 5"/></svg>                        
                        </div>
                        <div class="d-flex flex-column">
                            <h5 class="">Best Services</h5>
                            <p class="m-0" style="font-size:small">Elevate your Bhutan experience with our top-notch services, making every moment unforgettable.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex mb-4 mb-lg-0">
                        <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-primary mr-3" style="height: 100px; width: 100px;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.3em" height="2.3em" viewBox="0 0 64 64"><path fill="#ffce31" d="M17 6C2.7 14.3-2.3 32.7 6 47l52-30C49.7 2.7 31.3-2.3 17 6"/><path fill="#ed4c5c" d="M47 58c14.3-8.3 19.3-26.6 11-41L6 47c8.3 14.3 26.7 19.3 41 11"/><path fill="#fff" d="M57.7 18.4s-1.5-.1-2.3-.1c-.8 0-1.5.5-2.4.6c.6-.3.9-.8 1.4-1.1c.5-.2 1.3-.3 1.4-1c-.5.4-1.2.3-1.8.2c-.6 0-1 .5-1.4.8c.2-.9.7-1.5.8-2.4c.1-.8 1.4-1.4 1-2.3c-.1.8-1.1.9-1.4 1.6c-.3.9-1.1 1.6-1.2 2.6c-.1 1.2-1 .6-1.8.9c-.2.1-1.1-1-2.4-1c-2 .1-2.8.1-5.4-2.5c-.6-.6-2.7-.8-3.5-.3c.8 0 1.5.2 2.1.6c-.4.3-1-.4-1.5-.1c.8 0 1.1.4 1 1.1c-.7-.5-1.9-1.2-2.8-.8c-.6.3-1.7-.1-2 .7c.8-.4 1.6-.1 2.4.2c-.4.1-1.4.1-1.7.5c.4-.1.8-.1 1.1 0c-.1.3-.2.6-.1.9c-1.1-.4-2.6-1-3.6-.2c-1.1.9.6 1.9.9 1.3c-.7.3-1-.9 0-.9c1.4-.1 2.7.5 3.9 1.1c-.6.2-2.5 0-2.8-.8c-.1.3-.1.7.2.9c-.9 0-1.7.9-2.5.8c-.7 0-2.1 0-2.2.9c1.1-1 2.6-.2 3.9-.4c-.3.6-.5 1.3-.7 2c-.3.9-1.2 1.3-1.7 2.2c.4-.4.9-.7 1.5-.8c-.3.7-1.1 1.8-.7 2.5c.2-1.1.9-.8 1.7-1.2c-.2.5.2 1.4.9 1.2c-.3.5 0 1.2.6 1.4c-.4.5-1.4.2-1.6.7c.7-.3 1.3.8 2.2.5c-.1.4 0 .7.2 1c-.5-.1-1.4.3-1.7-.4c-.3.8 1.1.6.9 1.2c-.3 0-.7-.8-.9-.2c-.1 0-.3-.6-.6-.7c-.4-.1-.9.2-1.1.1c.4-.6-.7-1-1.1-.6c.1-.9-1.6-1.5-1.6-2.2c-.3.5.3.9.2 1.4c-.2-.2-.4-.3-.6-.2c-.6.1-.2-.1-.8-.2c-.6-.1-.3.3-.8 0c-.3-.2-.7-.1-.8.1c-.3-1-1.6-.5-1.9-1.3c-.1.5.6 1 .7 1.5c-.6-.5-1.7.1-1.4.9c-.2.1-1.6-.4-1.3.7c-.4-.4-2.7-.1-2.7.6c.4-.3 1.2-.1 1.5.1c-.3.2-.4.9-.2 1.2c-1.1-.1-.6 1.4-1.1 1.7c-.7.4-1.4 1.4-2.3 1.1c.5.8 1.5 0 2.1.1c-.7.5-.1 1.3-.6 1.8c-.4.5-.9 1.7-1.8 1.4c.1.4.6.5 1 .5c-.3.2-.6.6-1 .4c0 .3.2.5.4.6c-.5 0-.9.2-1.4.1c0 .2.1.5.2.7c-.6-.2-1.8-.5-2.2.2c.3 0 .8 0 .9.4c-.4-.3-.9-.1-1.4-.1c-.7.1-1.1-.3-1.8-.5c-.5-.2-2.8-.6-2.9.2c.5-.4 1.6.4 2.2.3c.9-.1 1.6.6 2.4.8c-1.2-.1-1.9.5-3 .6c-.5.1-1.6-.1-1.7.6c.5-.3 1.1.1 1.7-.1c.5-.1 1-.5 1.5-.3c-.3.4-.8.6-1.3.7c-.8.1-1.1.4-1.8.8c-.5.3-1.2.2-1.7.6c-.4.3-.8.8-1.4.8c.7.4 1.4-.3 2-.5c.8-.3 1.6-.2 2.3-.7c-.4 1-1.8.8-2.6 1.4c-1 .7-1.7 1.8-2.9 2c.5.6 1.6-.2 2-.4c.9-.5 2-.3 2.8-1c.8-.7 1.7-.8 2.4-1.5c.2-.2 2-1.4 2.2-1.4c-.7.3-.8 1.4-1.7 1.6c.7.3 2.2-.9 3-1.2c-.4.6-1.7.9-1.8 1.6c.7-.6 1.7-.4 2.3-.9c.6-.5 1.8-1.3 2.2-2c.4-.6.8-.9 1.4-1.3c.3-.2.6-.5 1-.6c.4-.1.8.2 1.2 0c.1 1.8-.6.3-1.2 1.1c-.1.1.6.3.7.5c0 0-1.1 1-1.2 1c.3.4 1 .1 1.3-.2c-.1.3.3.6.2.9c.2 0 .7-1.1.9-.9c-.5.2.4.5.4.5c-.3.2 0 .5.2.4c-.3.1 0 .6-.2.8c-.2.5-.6.4-1.1.8c-.4.4-.5.9-.4 1.4c.1.4.4 1.7.6.9c-.4 1.9 2 3.1 3.4 1.8c.7-.7 1.5-3.6.3-4.2c.2.1.5 0 .6-.1c.1.2.2.3.4.3c.7.2.9 1.1.8 1.8c.5-.6.4-2.1-.1-2.7c-.2-.2-.9-.5-1.2-.6c-.3 0-.5 0-.6.3c-.2-.2-.5-.3-.8-.2c.1-.4-.4-1.4-.8-1.6c-.8-.5-.3-1.8-.1-2.5c.3.2 1.2.5 1.6.7c.4.2 2.6.7 1.7.3c2 .8 3.2-2.4 1.3-3.1c.8.1.1-.7-.3-.9c-.6-.2-1.8.1-2.3.5c.1-.1.3-.8.4-.9c.3-.3.6-.6 1.1-.4c-1.9-2.5-2.5 2.1-3.4 1.7c-.6-.3-.7-2-.4-2.4c.2-.2 0-.5.1-.8c.2-.3.6-.6.8-.9c.7-1.2 3.6-.6 4.9 0c1.7.7 4 1 5.8 1.2c-.2.4.1.6 0 .9c-.2-.1-1.8-.1-1.9 0c.4 0 .8.5 1.3.5c-.8.1-.9 1.2-1.5 1.2c.4.3 2.7-1.1 1.7.6c.4 0 .6-.5.7-.9c.4.2 2.8 2.2 3.3 1.4c0 .5.7.5 1 .5c.5 0 1.4.4 1.8.3c-1.5 1.1 2.6 4.6 2.6 3.2c0-.9 1.3.8 2.6-.4c1.3-1.3.2-3.8-1.6-3.7c.5-.1 1.6-.2 1.8.7c.9-1.5-1.5-2.3-2.5-1.9c.6-.3.2-.6.7-.9c.4-.2.7-.6 1.2-.5c-.6-.7-3-.5-2.8.7c-.3-.1-.5 0-.7.2c0 .4-.2.5-.6.5c-.2.4-.4.5-.6.1c-.4-.2-.6-.1-1-.4c-.3-.2-1-.2-1.3-.2c-.5.1-.7-.6-1.3-.4c-.1-1 .7-1.6 1.1-2.4c.5-.9 1-1.8 1.2-2.9c.1-.9-.2-1.7 1-1.8c.9-.1 1.1 1 .2 1.3c.4.3.9-.1 1.1-.5c-.3.6-.3 1.7.5 1.9c-.2-.5.4-.9.2-1.4c.4.2.8.6.8 1.1c.5-.6-.2-1.2-.2-1.8c.9.1 1.5-.2 2.2-.8c.8-.5.9-.5 1.5 0c1.1.8 3.5.6 4.2-.7c.3-.7 1-.7.4-1.5c-.4-.6-.9-1.1-1.2-1.7c-.8-1.9-3.3.2-3.6.8c-.4.6-.1.7-.9.6c-1-.1-1.9.6-3 .4c-1-.2-1.7-.7-2.5-1.2c-1.1-.6-1.7-.6-2.5-1.7c.6.1 1.3.2 1.8.7c.4.4.8 1 1.5.8c-.7-.2-1.4-1.3-2.1-1.7c.7-.9 2.2-.6 3.3-.6c.9 0 1.3 1 2 1.4c-.4.2-.5.8-1 .8c.4.2.9.1 1.3-.2c-.1.4 0 .8.4 1c-.3-.4.2-.7.4-.9c.1.3.5.5.4.8c1-.2.3-2.6-.4-2.8c1.3-1.1 1.6.9 1.2 1.8c.5-.4.5-1 .5-1.6c1 .3 5.8-1.1 5.6.8c.7-.8-.6-1.4-1.2-1.5c-.5-.1-.8-1.5-1.5-.9c.8-.1.4 1-.1 1.2c.2-.7-.5-1.2-1.2-1c1.7.5-.9 1.8-.7.9c.1-.6.6-1 .5-1.6c-.1-.3-.3-.7-.3-.9c.4.1.4-.2.1-.4c.3.1.6-.2.9-.2c.7-.1 1.3-.4 2-.7c1-.3 3.5-.7 3.5-.7m-11.3 3.3c.3-.7 1.9-1.4 1.4-.2c.2-.1.5-.8.8-.6c.4.2.6.6.1.8c.4 0 .8-.3 1.2-.2c-.2.2-.2.6-.5.8c-.6.4-2.1-.5-3-.6m1.8 1.6c-.4-.2-1.2-.6-1.5-1c.6.2 1.3.3 1.9.6c0 0-.3.2-.4.4"/></svg>                        </div>
                        <div class="d-flex flex-column">
                            <h5 class="">Bhutan Coverage</h5>
                            <p class="m-0" style="font-size:small">Discover every corner of Bhutan with our all-inclusive trips.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Feature End -->


    <!-- Destination Start -->
    <div id="destinations" class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-3 pb-3">
                <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">Destinations</h6>
                <h1 style="font-size:xx-large">Explore Top Destinations</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4" >
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/buddhapoint.jpg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white">Buddha Point</h5>
                            <span>Thimphu</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/dochulapass.jpg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white">Dochula Pass</h5>
                            <span>Punakha</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/punakha.jpg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white">Punakha Dzong</h5>
                            <span>Punakha</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/taktshang.jpg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white" style="text-align:center">Taktshang<br>The Tiger Nest</h5>
                            <span>Paro</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/punakharafting.jpg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white">Punatshangchhu Rafting</h5>
                            <span>Punakha, Punatshangchhu</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="destination-item position-relative overflow-hidden mb-2" style="height:205px">
                        <img class="img-fluid" src="img/memorialchorten.jpeg" alt="">
                        <a class="destination-overlay text-white text-decoration-none" href="">
                            <h5 class="text-white">Memorial Chorten</h5>
                            <span>Thimphu</span>
                        </a>
                    </div>
                </div>
            </div>
            <h6 class="text-primary text-camelcase" style="text-align:center"><a href="{{route('destinations')}}">View More</a></h6>
        </div>
    </div>
    <!-- Destination Start -->


    <!-- Service Start -->
    <div id="services" class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-3 pb-3">
                <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">Services</h6>
                <h1 style="font-size:xx-large">Tours & Travel Services</h1>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="service-item bg-white text-center mb-2 py-5 px-4">
                        <i class="fa fa-2x fa-route mx-auto mb-4"></i>
                        <h5 class="mb-2">Travel Guide</h5>
                        <p class="m-0">Expert local guidance for immersive experiences.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="service-item bg-white text-center mb-2 py-5 px-4">
                        <i class="fa fa-2x fa-ticket-alt mx-auto mb-4"></i>
                        <h5 class="mb-2">Ticket Booking</h5>
                        <p class="m-0">Hassle-free flight bookings at competitive rates.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="service-item bg-white text-center mb-2 py-5 px-4">
                        <i class="fa fa-2x fa-hotel mx-auto mb-4"></i>
                        <h5 class="mb-2">Hotel Booking</h5>
                        <p class="m-0">Find your perfect retreat with our curated accommodations.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="service-item bg-white text-center mb-2 py-5 px-4">
                        <i class="fa fa-2x fa-car mx-auto mb-4"></i>
                        <h5 class="mb-2">Car Rental</h5>
                        <p class="m-0">Unlock adventure with our reliable and flexible car rental service.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Service End -->


    <!-- Packages Start -->
    <div id="gallery" class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-3 pb-3">
                <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">Gallery</h6>
                <h1 style="font-size:xx-large">Explore Our Gallery</h1>
            </div>
            <div class="row">
                <!-- Gallery -->
                <div class="row">
                    <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                        <img
                        src="{{asset('img/1.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Boat on Calm Water"
                        />

                        <img
                        src="{{asset('img/2.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Wintry Mountain Landscape"
                        />
                    </div>

                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <img
                        src="{{asset('img/3.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Mountains in the Clouds"
                        />

                        <img
                        src="{{asset('img/4.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Boat on Calm Water"
                        />
                    </div>

                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <img
                        src="{{asset('img/5.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Waves at Sea"
                        />

                        <img
                        src="{{asset('img/6.jpeg')}}"
                        class="w-100 shadow-1-strong rounded mb-4"
                        alt="Yosemite National Park"
                        />
                    </div>
                </div>
            <!-- Gallery -->
            </div>
            <h6 class="text-primary text-camelcase" style="text-align:center"><a href="{{route('gallery')}}">View More</a></h6>

        </div>
    </div>
    <!-- Packages End -->


   


    <!-- Team Start -->
    <div class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-3 pb-3">
                <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">Guides</h6>
                <h1 style="font-size:xx-large">Our Travel Guides</h1>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-4 col-sm-6 pb-2">
                    <div class="team-item bg-white mb-4">
                        <div class="team-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="img/PemaWangda.jpeg" alt="">
                            <!-- <div class="team-social">
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-instagram"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-linkedin-in"></i></a>
                            </div> -->
                        </div>
                        <div class="text-center py-4">
                            <h5 class="text-truncate">Pema Wangda</h5>
                            <p class="m-0">Guide</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 pb-2">
                    <div class="team-item bg-white mb-4">
                        <div class="team-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="img/PhuntshoDorji.jpeg" alt="">
                            <!-- <div class="team-social">
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-instagram"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-linkedin-in"></i></a>
                            </div> -->
                        </div>
                        <div class="text-center py-4">
                            <h5 class="text-truncate">Phuntsho Dorji</h5>
                            <p class="m-0">Guide</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 pb-2">
                    <div class="team-item bg-white mb-4">
                        <div class="team-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="img/Tshering Samdrup.jpeg" alt="">
                            <!-- <div class="team-social">
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-instagram"></i></a>
                                <a class="btn btn-outline-primary btn-square" href=""><i class="fab fa-linkedin-in"></i></a>
                            </div> -->
                        </div>
                        <div class="text-center py-4">
                            <h5 class="text-truncate">Tshering Samdrup</h5>
                            <p class="m-0">Guide</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team End -->

     <!-- Feedback Start -->
     <div id="feedback" class="container-fluid bg-registration py-5" style="margin: 90px 0;">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-5 mb-lg-0">
                    <div class="mb-4">
                        <h1 class="text-white">Your Voice, Our Growth</h1>
                    </div>
                    <p class="text-white">We value your thoughts and experiences. Share your feedback with us to help us continually enhance our services and tailor your experience to perfection. Let your voice shape our journey together.</p>
                    
                </div>
                <div class="col-lg-5">
                    <div class="card border-0">
                        <div class="card-header bg-primary text-center p-4">
                            <h3 class="text-white m-0">Your Feedback</h3>
                        </div>
                        <div class="card-body rounded-bottom bg-white p-5">
                            <form action="{{route('sendFeedback')}}" method="POST">
                                @csrf
                                <div style="width:100%;margin-bottom:10px">
                                    <input style="width:100%" placeholder="Fullname" name="name" type="text" id="small-input" class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                </div>
                                <div style="width:100%;margin-bottom:10px">
                                    <input style="width:100%" placeholder="Email" name="email" type="email" id="small-input" class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                </div>
                                <textarea style="width:100%;margin-bottom:10px;padding-inline:10px" name="message" id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Leave a feedback..."></textarea>
                                <div>
                                    <button type="submit" style="background-color:#7ab730" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Registration End -->


    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-white border-top py-4 px-sm-3 px-md-5" style="border-color: rgba(256, 256, 256, .1) !important;">
        <div class="row">
            <div class="col-lg-6 text-center text-md-left mb-3 mb-md-0">
                <h1 class="text-primary"><span class="text-white">Ka-</span>Nying</h1>
                <span>Networking Tours and Travels</span>
                <p class="m-0 mt-2 text-white-50">Copyright &copy; 2024. All Rights Reserved.</a>
                </p>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <!-- Success raised circle button with ripple effect -->

    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>
    <a href="https://wa.link/hslp9n" class="btn btn-lg btn-primary btn-lg-square whatsapp"><i class="fab fa-whatsapp"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('lib/easing/easing.min.js')}}"></script>
    <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('lib/tempusdominus/js/moment.min.js')}}"></script>
    <script src="{{asset('lib/tempusdominus/js/moment-timezone.min.js')}}"></script>
    <script src="{{asset('lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js')}}"></script>

    <!-- Contact Javascript File -->
    <script src="{{asset('mail/jqBootstrapValidation.min.js')}}"></script>
    <script src="{{asset('mail/contact.js')}}"></script>

    <!-- Template Javascript -->
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>


    <!-- jQuery first, then Popper.js, then Bootstrap JS, then Propeller js -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	  <script type="text/javascript" src="js/propeller.min.js"></script>
    @if(Session::has('success'))
            <script>
                swal("Message", "{{Session::get('success')}}",'success',{
                    button:true,
                    button:"OK",
                    timer:3000,
                })
            </script>
        @endif
</body>

</html>